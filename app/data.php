<?php // model
$resume = [];


// Возраст
$bDate = getBdate($doc);
$datetime1 = new DateTime(getBdate($doc));
$datetime2 = new DateTime(date('c'));
$interval = $datetime1->diff($datetime2);
$years = $interval->format('%y%');

$resume['age'] = [
  '@bdate'      => getBdate($doc),
  '@years'      => $years
];


// Зарплата
$salary = getSalary($doc);
$salary = empty($salary) ? 'не указано' : $salary;

$resume['salary'] = [
  '@value'      => number_format(getSalary($doc), 0, ',', ' ')
];


// Навыки
$resume['skills'] = getSkills($doc);


// Опыт
$resume['exp'] = [
  '@interval'   => getExp($doc)
];


// Опыт
$resume['works'] = [
  '@count'      => getWorksCount($doc)
];