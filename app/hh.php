<?php // controller
$dom = new SimpleXMLElement('<?xml version="1.0"?><app></app>');

if (isset($_GET['url']) && !empty($_GET['url'])) {
  $doc = getPage($_GET['url']);

  if ($doc) {
    require_once realpath(__APP__ . '/data.php');

    $resume = array('resume' => $resume);
    array_to_xml($resume, $dom);
  }
}

echo xslt($dom->saveXML(), __APP__ . '/html.xsl');

exit(0);