<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" version="5" doctype-system="about:legacy-compat" encoding="UTF-8" include-content-type="no" indent="no" />

  <xsl:template match="/">

    <html class="html" lang="ru-RU">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- <link rel="icon" href="/favicon.png" sizes="16x16" /> -->

        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Material+Icons|Fira+Sans:400,500,400i&amp;subset=cyrillic" />
        <link rel="stylesheet" href="/common.css" />

        <!-- <script src="//yastatic.net/jquery/3.1.1/jquery.min.js"></script> -->
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script> -->
        <!-- <script src="/scripts/libs/jquery-3.2.1.min.js"></script> -->
        <!-- <script src="/scripts/common.js"></script> -->
        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <![endif]-->

        <title>Парсинг резюме с HeadHunter</title>
      </head>
      <body class="body">
        <div class="section main">
          <div class="section__content section__content_fixed_864">
            <div class="grid">

              <div class="grid__col grid__col_12">
                <div class="grid__content grid__content_gutter_24">

                  <form class="form" action="/" method="get">
                    <input class="form__field form__field_state_focus" name="url" placeholder="Вставьте ссылку на резюме HeadHunter" autocomplete="off" />
                    <button class="form__go form__go_state_hover" type="submit"><i class="icon">arrow_forward</i></button>
                  </form>

                </div>
              </div>

              <xsl:if test="/app/resume">
                <div class="grid__col grid__col_12">
                  <div class="grid__content grid__content_gutter_24">

                    <div class="resume">
                      <div class="grid">

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Возраст
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <xsl:choose>
                                    <xsl:when test="/app/resume/age/@years = 0">
                                      <div class="resume__text resume__text_hidden">
                                        <xsl:text>Не указан</xsl:text>
                                      </div>
                                    </xsl:when>
                                    <xsl:when test="/app/resume/age/@years > 0">
                                      <div class="resume__text">
                                        <xsl:value-of select="/app/resume/age/@years" />
                                      </div>
                                    </xsl:when>
                                  </xsl:choose>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Ожидание по ЗП
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <xsl:choose>
                                    <xsl:when test="/app/resume/salary/@value = 0">
                                      <div class="resume__text resume__text_hidden">
                                        <xsl:text>Не указано</xsl:text>
                                      </div>
                                    </xsl:when>
                                    <xsl:when test="/app/resume/salary/@value != 0">
                                      <div class="resume__text">
                                        <xsl:value-of select="/app/resume/salary/@value" /> руб.
                                      </div>
                                    </xsl:when>
                                  </xsl:choose>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Навыки
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <xsl:if test="count(/app/resume/skills/item) = 0">
                                    <div class="resume__text resume__text_hidden">
                                      <xsl:text>Не указаны</xsl:text>
                                    </div>
                                  </xsl:if>
                                  <xsl:apply-templates select="/app/resume/skills/item" />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Опыт работы
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text">
                                    <xsl:value-of select="/app/resume/exp/@interval" /> мес.
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Кол-во рабочих мест
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text">
                                    <xsl:value-of select="/app/resume/works/@count" />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="grid__col grid__col_12 resume__section">
                          <div class="grid__content grid__content_gutter_24">
                            <div class="grid">
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text resume__text_term">
                                    Средняя дл-ть работы на одном месте
                                  </div>
                                </div>
                              </div>
                              <div class="grid__col grid__col_6">
                                <div class="grid__content grid__content_gutter_24">
                                  <div class="resume__text">
                                    <xsl:value-of select="round(/app/resume/exp/@interval div /app/resume/works/@count)" /> мес.
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                  </div>
                </div>
              </xsl:if>

            </div>
          </div>
        </div>
      </body>
    </html>

  </xsl:template>

  <xsl:template match="item">
    <div class="resume__text">
      <xsl:value-of select="." />
    </div>
  </xsl:template>
</xsl:stylesheet>