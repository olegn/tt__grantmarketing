<?php
/**
 * Возвращает объект dom из html, полученного по url.
 * @param string $url
 * @return object
 */
function getPage(string $url) {
  // $host = parse_url($url, PHP_URL_HOST);
  $_url = parse_url($url);
  $dom = null;
  //7901a302000104e3900039ed1f616a49436c6b
  if (preg_match('/\/resume\/[a-z0-9]{38}/i', $_url['path'])) {

    // Если host не определен,
    // значит дальше продолжать нет смысла
    if (empty($_url['host']) || $_url['host'] === false) {
      trigger_error('Невозможно определить host из адреса', E_USER_WARNING);
    }

    // Подготовить заголовки для запроса
    $opts = array(
      'http' => array(
        'method' => 'GET',
        'header' => "Host: " . $_url['host'] . "\r\n"
          . "Accept: text/html\r\n"
          . "Accept-Language: ru\r\n"
          . "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15\r\n"
      )
    );
    $context = stream_context_create($opts);

    // Получить содержимое страницы
    $page = file_get_contents($url, false, $context);

    // Преобразовать html в объект, если требуется
    $doc = new DOMDocument();
    $doc->loadHTML($page);
  }

  return $doc;
}


/**
 * Найти и вернуть дату в формате iso.
 * @param object $doc
 * @return string
 */
function getBdate(object $doc) {
  $elems = $doc->getElementsByTagName('meta');
  $return = '';

  foreach ($elems as $elem) {
    if ($elem->hasAttribute('itemprop') && $elem->getAttribute('itemprop') == 'birthDate') {
      $return = $elem->getAttribute('content');
    }
  }

  return $return;
}


/**
 * Найти и вернуть ожидаемую ЗП.
 * span(data-qa="resume-block-salary")
 * @param object $doc
 * @return int
 */
function getSalary(object $doc) {
  // var_dump('90 000'); <= пробел
  $elems = $doc->getElementsByTagName('span');
  $return = '';

  foreach ($elems as $elem) {
    if ($elem->hasAttribute('data-qa') && $elem->getAttribute('data-qa') == 'resume-block-salary') {
      $return = (int) str_replace(array("\t", ' ', ' ', '&nbsp;', 'руб.'), '', $elem->nodeValue);
    }
  }

  return $return;
}


/**
 * Найти и вернуть тэги с навыками.
 * span(data-qa="bloko-tag__text")
 * @param object $doc
 * @return array
 */
function getSkills(object $doc) {
  $elems = $doc->getElementsByTagName('span');
  $return = array();

  foreach ($elems as $elem) {
    if ($elem->hasAttribute('data-qa') && $elem->getAttribute('data-qa') == 'bloko-tag__text') {
      $return[] = $elem->nodeValue;
    }
  }

  return $return;
}


/**
 * Найти, рассчитать и вернуть кол-во отработанных месяцев.
 * div(data-qa="resume-block-experience")>h2
 * @param object $doc
 * @return int
 */
function getExp(object $doc) {
  $elems = $doc->getElementsByTagName('div');

  foreach ($elems as $elem) {
    if ($elem->hasAttribute('data-qa') && $elem->getAttribute('data-qa') == 'resume-block-experience') {
      preg_match('/(?:([0-9]{1,})\s(?:лет|год|года)\s)?([0-9]{1,})\sмесяц/i', $elem->getElementsByTagName('h2')[0]->nodeValue, $matches);
    }
  }

  return $matches[1] * 12 + $matches[2];
}


/**
 * Подсчиттать и вернуть кол-во мест работы.
 * div(itemprop="worksFor")
 * @param object $doc
 * @return int
 */
function getWorksCount(object $doc) {
  $elems = $doc->getElementsByTagName('div');
  $i = 0;

  foreach ($elems as $elem) {
    if ($elem->hasAttribute('itemprop') && $elem->getAttribute('itemprop') == 'worksFor') {
      $i++;
    }
  }

  return $i;
}


/**
 * Конвертор массива в XML дерево.
 * @param (array)  $data
 * @param (object) $xml_data
 * @param (string) $key_num
 * @return void
 */
function array_to_xml($data, &$xml_data, $key_num = 'item') {
  foreach($data as $key => $value) {
    if(strpos($key, '@') === 0){
      $key = substr($key, 1);
      $xml_data->addAttribute($key, $value);
    } else {
      $key = explode(':', $key);

      if (isset($key[1]) && !empty($key[1])) {
        $key_num = $key[1];
      } /* else {
        $key_num = 'item';
      } */

      if(is_numeric($key[0])){
        $key = $key_num;
      } else {
        $key = $key[0];
      }

      if(is_array($value)) {
        $subnode = $xml_data->addChild($key);
        array_to_xml($value, $subnode, $key_num);
      } else {
        $xml_data->addChild($key, $value);
      }
    }
  }
}

/**
 * Обработчик XSL шаблона.
 * @param (string) $xml_doc
 * @param (string) $xsl_doc
 * @return string
 */
function xslt($xml_doc, $xsl_doc) {
  $xml = new DOMDocument;
  if ($xml_doc_file = realpath($xml_doc)) {
    $xml->load($xml_doc_file);
  } else {
    $xml->loadXML($xml_doc);
  }

  $xsl = new DOMDocument;
  $xsl->load($xsl_doc);

  $proc = new XSLTProcessor;

  $proc->importStyleSheet($xsl);

  return $proc->transformToXML($xml);
}